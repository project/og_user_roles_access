
-- SUMMARY --
Organic group user roles access reuses much of ogur functionality. Additionally it provides further

-- REQUIREMENTS --
OGUR 4.0 (organic groups user roles)
http://drupal.org/project/og_user_roles


-- INSTALLATION --
Enable the module.

-- CONFIGURATION --
All enable OGUR roles are candidate for OGURA content access realms.
After activation ofa OGURA realm role, nodetype specific settings appear.

You need to override og_subscriber permissions and allow view permission for your chosen OGURA role.

-- USAGE --


-- NOTES --


-- CONTACT --

* MD Systems
  Drupal & Development
  Visit http://www.md-systems.ch for more information.


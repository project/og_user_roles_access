<?php

/**
 * @file
 * Page callbacks for OG user roles access.
 */

/**
 * Form builder for administrative settings.
 */
function og_user_roles_access_admin_settings() {
  $form = array();

  $form['#submit'][] = 'og_user_roles_access_admin_settings_submit';

  // Get list of all og-enabled node types.
  $group_post_types = og_get_types('group_post');
  $types = array();
  foreach ($group_post_types as $type) {
    $types[$type] = node_get_types('name', $type);
  }
  
  // Get list of roles, not counting authenticated and anonymous user.
  $all_roles = user_roles();
  // extract ogur enabled roles!
  $all_ogur_roles = og_user_roles_access_ogur_roles();

  // only ogur enabled roles!
  foreach ($all_roles as $rid => $role) {
    if ($rid > DRUPAL_AUTHENTICATED_RID
      && isset($all_ogur_roles[$rid])) {
      $roles[$rid] = $role;
    }
  }

  $form['og_user_roles_access_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles for content access realms'),
    '#description' => t('Only ogur enabled roles allowed!'),
    '#options' => $roles,
    '#default_value' => variable_get("og_user_roles_access_roles", array()),
    );

  $roles_enabled = array_filter(variable_get("og_user_roles_access_roles", array()));

  // only continue after role activation
  if(count($roles_enabled)==0) {
    return system_settings_form($form);
  }

  foreach ($types as $type => $typename) {
    // per group post type
    $typekey = 'og_user_roles_access_perm_' . $type;
    $typeoverride = $typekey . '_override';
    $typeused = variable_get($typeoverride, 0);
    $form[$typekey] = array(
      '#type' => 'fieldset',
      '#title' => $typename,
      '#collapsible' => TRUE,
      '#collapsed' => !$typeused,
    );
    $form[$typekey][$typeoverride] = array(
      '#type' => 'checkbox',
      '#title' => t('Override OG Subscriber records'),
      '#description' => t('You need to override records in order to apply ogur specific node access permissions.'),
      '#default_value' => variable_get($typeoverride, 0),
    );
    foreach ($roles_enabled as $rid) {
      // per ogura enabled role
      $role = $roles[$rid];
      $typeviewrole = $typekey . '_view_' . $rid;
      $form[$typekey][$typeviewrole] = array(
        '#type' => 'checkbox',
        '#title' => t('View ' . $role),
        '#default_value' => variable_get($typeviewrole, 0),
      );
    }
  }

  return system_settings_form($form);
}

function og_user_roles_access_admin_settings_submit($form, &$form_state) {
  // CHECK for change and notify for permission rebuild
}
